# Datacite API



## Generating the schema structs
### JSON
1. Download Datacite schema from https://github.com/datacite/schema/tree/master/source/json
2. Generate Go code from JSON schema (e.g. with https://github.com/a-h/generate)
### XML
1. Download Datacite schema from https://schema.datacite.org/
2. Generate Go code from XSD (e.g. with https://github.com/xuri/xgen)

1. git clone https://github.com/GoComply/xsd2go
2. add "language" in line 256 of pkg/xsd/types.go and compile: `go build -o xsd2go cli/gocomply_xsd2go/main.go`
3. git clone https://github.com/datacite/schema.git
4. `xsd2go convert ./schema/source/meta/kernel-4.4/metadata.xsd gitlab.ulb.tu-darmstadt.de/tittel/datacite .`
5. remove duplicates in generated models.go
6. replace this in Resource struct: XMLName xml.Name `xml:"http://datacite.org/schema/kernel-4 resource"`
7. add `,omitempty` for all XmlLang definitions, "Language" and "Version"
 
## Generating the API code
```
docker run --rm --user 1000:1000 -v "${PWD}:/local" openapitools/openapi-generator-cli generate \
    -i https://raw.githubusercontent.com/datacite/lupo/master/openapi.yaml \
    -g go \
    -p packageName=datacite \
    -o /local
```



