package schema

import (
	"encoding/xml"
)

// Element
type Resource struct {
	XMLName xml.Name `xml:"http://datacite.org/schema/kernel-4 resource"`

	Identifier ResourceIdentifier `xml:"identifier"`

	Creators ResourceCreators `xml:"creators"`

	Titles ResourceTitles `xml:"titles"`

	Publisher ResourcePublisher `xml:"publisher"`

	PublicationYear string `xml:"publicationYear"`

	ResourceType ResourceResourceType `xml:"resourceType"`

	Subjects *ResourceSubjects `xml:"subjects"`

	Contributors *ResourceContributors `xml:"contributors"`

	Dates *ResourceDates `xml:"dates"`

	Language string `xml:"language,omitempty"`

	AlternateIdentifiers *ResourceAlternateIdentifiers `xml:"alternateIdentifiers"`

	RelatedIdentifiers *ResourceRelatedIdentifiers `xml:"relatedIdentifiers"`

	Sizes *ResourceSizes `xml:"sizes"`

	Formats *ResourceFormats `xml:"formats"`

	Version string `xml:"version,omitempty"`

	RightsList *ResourceRightsList `xml:"rightsList"`

	Descriptions *ResourceDescriptions `xml:"descriptions"`

	GeoLocations *ResourceGeoLocations `xml:"geoLocations"`

	FundingReferences *ResourceFundingReferences `xml:"fundingReferences"`

	RelatedItems *ResourceRelatedItems `xml:"relatedItems"`
}

// Element
type ResourceIdentifier struct {
	XMLName xml.Name `xml:"identifier"`

	IdentifierType string `xml:"identifierType,attr"`

	Text string `xml:",chardata"`
}

// Element
type CreatorCreatorName struct {
	XMLName xml.Name `xml:"creatorName"`

	NameType NameType `xml:"nameType,attr,omitempty"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceCreators struct {
	XMLName xml.Name `xml:"creators"`

	Creator []CreatorsCreator `xml:",any"`
}

// Element
type ResourceTitles struct {
	XMLName xml.Name `xml:"titles"`

	Title []TitlesTitle `xml:",any"`
}

// Element
type ResourcePublisher struct {
	XMLName xml.Name `xml:"publisher"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceResourceType struct {
	XMLName xml.Name `xml:"resourceType"`

	ResourceTypeGeneral ResourceType `xml:"resourceTypeGeneral,attr"`

	Text string `xml:",chardata"`
}

// Element
type SubjectsSubject struct {
	XMLName xml.Name `xml:"subject"`

	SubjectScheme string `xml:"subjectScheme,attr,omitempty"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	ValueURI string `xml:"valueURI,attr,omitempty"`

	ClassificationCode string `xml:"classificationCode,attr,omitempty"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceSubjects struct {
	XMLName xml.Name `xml:"subjects"`

	Subject []SubjectsSubject `xml:",any"`
}

// Element
type ResourceContributors struct {
	XMLName xml.Name `xml:"contributors"`

	Contributor []ContributorsContributor `xml:",any"`
}

// Element
type DatesDate struct {
	XMLName xml.Name `xml:"date"`

	DateType DateType `xml:"dateType,attr"`

	DateInformation string `xml:"dateInformation,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceDates struct {
	XMLName xml.Name `xml:"dates"`

	Date []DatesDate `xml:",any"`
}

// Element
type AlternateIdentifiersAlternateIdentifier struct {
	XMLName xml.Name `xml:"alternateIdentifier"`

	AlternateIdentifierType string `xml:"alternateIdentifierType,attr"`

	Text string `xml:",chardata"`
}

// Element
type ResourceAlternateIdentifiers struct {
	XMLName xml.Name `xml:"alternateIdentifiers"`

	AlternateIdentifier []AlternateIdentifiersAlternateIdentifier `xml:",any"`
}

// Element
type RelatedIdentifiersRelatedIdentifier struct {
	XMLName xml.Name `xml:"relatedIdentifier"`

	ResourceTypeGeneral ResourceType `xml:"resourceTypeGeneral,attr,omitempty"`

	RelatedIdentifierType RelatedIdentifierType `xml:"relatedIdentifierType,attr"`

	RelationType RelationType `xml:"relationType,attr"`

	RelatedMetadataScheme string `xml:"relatedMetadataScheme,attr,omitempty"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	SchemeType string `xml:"schemeType,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceRelatedIdentifiers struct {
	XMLName xml.Name `xml:"relatedIdentifiers"`

	RelatedIdentifier []RelatedIdentifiersRelatedIdentifier `xml:",any"`
}

// Element
type ResourceSizes struct {
	XMLName xml.Name `xml:"sizes"`

	Size []string `xml:",any"`
}

// Element
type ResourceFormats struct {
	XMLName xml.Name `xml:"formats"`

	Format []string `xml:",any"`
}

// Element
type RightsListRights struct {
	XMLName xml.Name `xml:"rights"`

	RightsURI string `xml:"rightsURI,attr,omitempty"`

	RightsIdentifier string `xml:"rightsIdentifier,attr,omitempty"`

	RightsIdentifierScheme string `xml:"rightsIdentifierScheme,attr,omitempty"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ResourceRightsList struct {
	XMLName xml.Name `xml:"rightsList"`

	Rights []RightsListRights `xml:",any"`
}

// Element
type DescriptionBr struct {
	XMLName xml.Name `xml:"br"`
}

// Element
type DescriptionsDescription struct {
	XMLName xml.Name `xml:"description"`

	DescriptionType DescriptionType `xml:"descriptionType,attr"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Br []DescriptionBr `xml:"br"`
}

// Element
type ResourceDescriptions struct {
	XMLName xml.Name `xml:"descriptions"`

	Description []DescriptionsDescription `xml:",any"`
}

// Element
type GeoLocationGeoLocationPolygon struct {
	XMLName xml.Name `xml:"geoLocationPolygon"`

	PolygonPoint []Point `xml:"polygonPoint"`

	InPolygonPoint *Point `xml:"inPolygonPoint"`
}

// Element
type GeoLocationsGeoLocation struct {
	XMLName xml.Name `xml:"geoLocation"`

	GeoLocationPlace []string `xml:"geoLocationPlace"`

	GeoLocationPoint []Point `xml:"geoLocationPoint"`

	GeoLocationBox []Box `xml:"geoLocationBox"`

	GeoLocationPolygon []GeoLocationGeoLocationPolygon `xml:"geoLocationPolygon"`
}

// Element
type ResourceGeoLocations struct {
	XMLName xml.Name `xml:"geoLocations"`

	GeoLocation []GeoLocationsGeoLocation `xml:",any"`
}

// Element
type FundingReferenceFunderIdentifier struct {
	XMLName xml.Name `xml:"funderIdentifier"`

	FunderIdentifierType FunderIdentifierType `xml:"funderIdentifierType,attr"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type FundingReferenceAwardNumber struct {
	XMLName xml.Name `xml:"awardNumber"`

	AwardURI string `xml:"awardURI,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type FundingReferencesFundingReference struct {
	XMLName xml.Name `xml:"fundingReference"`

	FunderName string `xml:"funderName"`

	FunderIdentifier *FundingReferenceFunderIdentifier `xml:"funderIdentifier"`

	AwardNumber *FundingReferenceAwardNumber `xml:"awardNumber"`

	AwardTitle string `xml:"awardTitle"`
}

// Element
type ResourceFundingReferences struct {
	XMLName xml.Name `xml:"fundingReferences"`

	FundingReference []FundingReferencesFundingReference `xml:",any"`
}

// Element
type RelatedItemRelatedItemIdentifier struct {
	XMLName xml.Name `xml:"relatedItemIdentifier"`

	RelatedItemIdentifierType RelatedIdentifierType `xml:"relatedItemIdentifierType,attr,omitempty"`

	RelatedMetadataScheme string `xml:"relatedMetadataScheme,attr,omitempty"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	SchemeType string `xml:"schemeType,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type CreatorsCreator struct {
	XMLName xml.Name `xml:"creator"`

	CreatorName CreatorCreatorName `xml:"creatorName"`

	GivenName string `xml:"givenName"`

	FamilyName string `xml:"familyName"`
}

// Element
type RelatedItemCreators struct {
	XMLName xml.Name `xml:"creators"`

	Creator []CreatorsCreator `xml:",any"`
}

// Element
type TitlesTitle struct {
	XMLName xml.Name `xml:"title"`

	TitleType TitleType `xml:"titleType,attr,omitempty"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type RelatedItemTitles struct {
	XMLName xml.Name `xml:"titles"`

	Title []TitlesTitle `xml:",any"`
}

// Element
type RelatedItemNumber struct {
	XMLName xml.Name `xml:"number"`

	NumberType NumberType `xml:"numberType,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ContributorContributorName struct {
	XMLName xml.Name `xml:"contributorName"`

	NameType NameType `xml:"nameType,attr,omitempty"`

	XmlLang string `xml:"lang,attr,omitempty"`

	Text string `xml:",chardata"`
}

// Element
type ContributorsContributor struct {
	XMLName xml.Name `xml:"contributor"`

	ContributorType ContributorType `xml:"contributorType,attr"`

	ContributorName ContributorContributorName `xml:"contributorName"`

	GivenName string `xml:"givenName"`

	FamilyName string `xml:"familyName"`
}

// Element
type RelatedItemContributors struct {
	XMLName xml.Name `xml:"contributors"`

	Contributor []ContributorsContributor `xml:",any"`
}

// Element
type RelatedItemsRelatedItem struct {
	XMLName xml.Name `xml:"relatedItem"`

	RelatedItemType ResourceType `xml:"relatedItemType,attr"`

	RelationType RelationType `xml:"relationType,attr"`

	RelatedItemIdentifier *RelatedItemRelatedItemIdentifier `xml:"relatedItemIdentifier"`

	Creators *RelatedItemCreators `xml:"creators"`

	Titles *RelatedItemTitles `xml:"titles"`

	PublicationYear string `xml:"publicationYear"`

	Volume string `xml:"volume"`

	Issue string `xml:"issue"`

	Number *RelatedItemNumber `xml:"number"`

	FirstPage string `xml:"firstPage"`

	LastPage string `xml:"lastPage"`

	Publisher string `xml:"publisher"`

	Edition string `xml:"edition"`

	Contributors *RelatedItemContributors `xml:"contributors"`
}

// Element
type ResourceRelatedItems struct {
	XMLName xml.Name `xml:"relatedItems"`

	RelatedItem []RelatedItemsRelatedItem `xml:",any"`
}

// XSD ComplexType declarations

type NameIdentifier struct {
	XMLName xml.Name

	NameIdentifierScheme string `xml:"nameIdentifierScheme,attr"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	Text     string `xml:",chardata"`
	InnerXml string `xml:",innerxml"`
}

type Affiliation struct {
	XMLName xml.Name

	AffiliationIdentifier string `xml:"affiliationIdentifier,attr,omitempty"`

	AffiliationIdentifierScheme string `xml:"affiliationIdentifierScheme,attr,omitempty"`

	SchemeURI string `xml:"schemeURI,attr,omitempty"`

	Text     string `xml:",chardata"`
	InnerXml string `xml:",innerxml"`
}

type Point struct {
	XMLName xml.Name

	PointLongitude LongitudeType `xml:"pointLongitude"`

	PointLatitude LatitudeType `xml:"pointLatitude"`

	InnerXml string `xml:",innerxml"`
}

type Box struct {
	XMLName xml.Name

	WestBoundLongitude LongitudeType `xml:"westBoundLongitude"`

	EastBoundLongitude LongitudeType `xml:"eastBoundLongitude"`

	SouthBoundLatitude LatitudeType `xml:"southBoundLatitude"`

	NorthBoundLatitude LatitudeType `xml:"northBoundLatitude"`

	InnerXml string `xml:",innerxml"`
}

// XSD SimpleType declarations

type NumberType string

const NumberTypeArticle NumberType = "Article"

const NumberTypeChapter NumberType = "Chapter"

const NumberTypeReport NumberType = "Report"

const NumberTypeOther NumberType = "Other"

type NameType string

const NameTypeOrganizational NameType = "Organizational"

const NameTypePersonal NameType = "Personal"

type DescriptionType string

const DescriptionTypeAbstract DescriptionType = "Abstract"

const DescriptionTypeMethods DescriptionType = "Methods"

const DescriptionTypeSeriesinformation DescriptionType = "SeriesInformation"

const DescriptionTypeTableofcontents DescriptionType = "TableOfContents"

const DescriptionTypeTechnicalinfo DescriptionType = "TechnicalInfo"

const DescriptionTypeOther DescriptionType = "Other"

type FunderIdentifierType string

const FunderIdentifierTypeIsni FunderIdentifierType = "ISNI"

const FunderIdentifierTypeGrid FunderIdentifierType = "GRID"

const FunderIdentifierTypeRor FunderIdentifierType = "ROR"

const FunderIdentifierTypeCrossrefFunderId FunderIdentifierType = "Crossref Funder ID"

const FunderIdentifierTypeOther FunderIdentifierType = "Other"

type RelatedIdentifierType string

const RelatedIdentifierTypeArk RelatedIdentifierType = "ARK"

const RelatedIdentifierTypeArxiv RelatedIdentifierType = "arXiv"

const RelatedIdentifierTypeBibcode RelatedIdentifierType = "bibcode"

const RelatedIdentifierTypeDoi RelatedIdentifierType = "DOI"

const RelatedIdentifierTypeEan13 RelatedIdentifierType = "EAN13"

const RelatedIdentifierTypeEissn RelatedIdentifierType = "EISSN"

const RelatedIdentifierTypeHandle RelatedIdentifierType = "Handle"

const RelatedIdentifierTypeIgsn RelatedIdentifierType = "IGSN"

const RelatedIdentifierTypeIsbn RelatedIdentifierType = "ISBN"

const RelatedIdentifierTypeIssn RelatedIdentifierType = "ISSN"

const RelatedIdentifierTypeIstc RelatedIdentifierType = "ISTC"

const RelatedIdentifierTypeLissn RelatedIdentifierType = "LISSN"

const RelatedIdentifierTypeLsid RelatedIdentifierType = "LSID"

const RelatedIdentifierTypePmid RelatedIdentifierType = "PMID"

const RelatedIdentifierTypePurl RelatedIdentifierType = "PURL"

const RelatedIdentifierTypeUpc RelatedIdentifierType = "UPC"

const RelatedIdentifierTypeUrl RelatedIdentifierType = "URL"

const RelatedIdentifierTypeUrn RelatedIdentifierType = "URN"

const RelatedIdentifierTypeW3Id RelatedIdentifierType = "w3id"

type RelationType string

const RelationTypeIscitedby RelationType = "IsCitedBy"

const RelationTypeCites RelationType = "Cites"

const RelationTypeIssupplementto RelationType = "IsSupplementTo"

const RelationTypeIssupplementedby RelationType = "IsSupplementedBy"

const RelationTypeIscontinuedby RelationType = "IsContinuedBy"

const RelationTypeContinues RelationType = "Continues"

const RelationTypeIsnewversionof RelationType = "IsNewVersionOf"

const RelationTypeIspreviousversionof RelationType = "IsPreviousVersionOf"

const RelationTypeIspartof RelationType = "IsPartOf"

const RelationTypeHaspart RelationType = "HasPart"

const RelationTypeIspublishedin RelationType = "IsPublishedIn"

const RelationTypeIsreferencedby RelationType = "IsReferencedBy"

const RelationTypeReferences RelationType = "References"

const RelationTypeIsdocumentedby RelationType = "IsDocumentedBy"

const RelationTypeDocuments RelationType = "Documents"

const RelationTypeIscompiledby RelationType = "IsCompiledBy"

const RelationTypeCompiles RelationType = "Compiles"

const RelationTypeIsvariantformof RelationType = "IsVariantFormOf"

const RelationTypeIsoriginalformof RelationType = "IsOriginalFormOf"

const RelationTypeIsidenticalto RelationType = "IsIdenticalTo"

const RelationTypeHasmetadata RelationType = "HasMetadata"

const RelationTypeIsmetadatafor RelationType = "IsMetadataFor"

const RelationTypeReviews RelationType = "Reviews"

const RelationTypeIsreviewedby RelationType = "IsReviewedBy"

const RelationTypeIsderivedfrom RelationType = "IsDerivedFrom"

const RelationTypeIssourceof RelationType = "IsSourceOf"

const RelationTypeDescribes RelationType = "Describes"

const RelationTypeIsdescribedby RelationType = "IsDescribedBy"

const RelationTypeHasversion RelationType = "HasVersion"

const RelationTypeIsversionof RelationType = "IsVersionOf"

const RelationTypeRequires RelationType = "Requires"

const RelationTypeIsrequiredby RelationType = "IsRequiredBy"

const RelationTypeObsoletes RelationType = "Obsoletes"

const RelationTypeIsobsoletedby RelationType = "IsObsoletedBy"

type ResourceType string

const ResourceTypeAudiovisual ResourceType = "Audiovisual"

const ResourceTypeBook ResourceType = "Book"

const ResourceTypeBookchapter ResourceType = "BookChapter"

const ResourceTypeCollection ResourceType = "Collection"

const ResourceTypeComputationalnotebook ResourceType = "ComputationalNotebook"

const ResourceTypeConferencepaper ResourceType = "ConferencePaper"

const ResourceTypeConferenceproceeding ResourceType = "ConferenceProceeding"

const ResourceTypeDatapaper ResourceType = "DataPaper"

const ResourceTypeDataset ResourceType = "Dataset"

const ResourceTypeDissertation ResourceType = "Dissertation"

const ResourceTypeEvent ResourceType = "Event"

const ResourceTypeImage ResourceType = "Image"

const ResourceTypeInteractiveresource ResourceType = "InteractiveResource"

const ResourceTypeJournal ResourceType = "Journal"

const ResourceTypeJournalarticle ResourceType = "JournalArticle"

const ResourceTypeModel ResourceType = "Model"

const ResourceTypeOutputmanagementplan ResourceType = "OutputManagementPlan"

const ResourceTypePeerreview ResourceType = "PeerReview"

const ResourceTypePhysicalobject ResourceType = "PhysicalObject"

const ResourceTypePreprint ResourceType = "Preprint"

const ResourceTypeReport ResourceType = "Report"

const ResourceTypeService ResourceType = "Service"

const ResourceTypeSoftware ResourceType = "Software"

const ResourceTypeSound ResourceType = "Sound"

const ResourceTypeStandard ResourceType = "Standard"

const ResourceTypeText ResourceType = "Text"

const ResourceTypeWorkflow ResourceType = "Workflow"

const ResourceTypeOther ResourceType = "Other"

type DateType string

const DateTypeAccepted DateType = "Accepted"

const DateTypeAvailable DateType = "Available"

const DateTypeCollected DateType = "Collected"

const DateTypeCopyrighted DateType = "Copyrighted"

const DateTypeCreated DateType = "Created"

const DateTypeIssued DateType = "Issued"

const DateTypeOther DateType = "Other"

const DateTypeSubmitted DateType = "Submitted"

const DateTypeUpdated DateType = "Updated"

const DateTypeValid DateType = "Valid"

const DateTypeWithdrawn DateType = "Withdrawn"

type ContributorType string

const ContributorTypeContactperson ContributorType = "ContactPerson"

const ContributorTypeDatacollector ContributorType = "DataCollector"

const ContributorTypeDatacurator ContributorType = "DataCurator"

const ContributorTypeDatamanager ContributorType = "DataManager"

const ContributorTypeDistributor ContributorType = "Distributor"

const ContributorTypeEditor ContributorType = "Editor"

const ContributorTypeHostinginstitution ContributorType = "HostingInstitution"

const ContributorTypeOther ContributorType = "Other"

const ContributorTypeProducer ContributorType = "Producer"

const ContributorTypeProjectleader ContributorType = "ProjectLeader"

const ContributorTypeProjectmanager ContributorType = "ProjectManager"

const ContributorTypeProjectmember ContributorType = "ProjectMember"

const ContributorTypeRegistrationagency ContributorType = "RegistrationAgency"

const ContributorTypeRegistrationauthority ContributorType = "RegistrationAuthority"

const ContributorTypeRelatedperson ContributorType = "RelatedPerson"

const ContributorTypeResearchgroup ContributorType = "ResearchGroup"

const ContributorTypeRightsholder ContributorType = "RightsHolder"

const ContributorTypeResearcher ContributorType = "Researcher"

const ContributorTypeSponsor ContributorType = "Sponsor"

const ContributorTypeSupervisor ContributorType = "Supervisor"

const ContributorTypeWorkpackageleader ContributorType = "WorkPackageLeader"

type TitleType string

const TitleTypeAlternativetitle TitleType = "AlternativeTitle"

const TitleTypeSubtitle TitleType = "Subtitle"

const TitleTypeTranslatedtitle TitleType = "TranslatedTitle"

const TitleTypeOther TitleType = "Other"

type NonemptycontentStringType string

type Edtf string

type YearType string

type LongitudeType float64

type LatitudeType float64
