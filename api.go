package datacite

import (
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"gitlab.ulb.tu-darmstadt.de/tittel/datacite/schema"
)

type DataciteClient struct {
	endpoint   string
	basicAuth  string
	httpClient *http.Client
}

type PublicationState string

const (
	Draft    PublicationState = "draft"
	Register PublicationState = "register"
	Publish  PublicationState = "publish"
	Hide     PublicationState = "hide"
)

func (c *DataciteClient) executeRequest(method string, path string, body io.Reader, authenticate bool) (map[string]interface{}, error) {
	req, err := http.NewRequest(method, fmt.Sprintf("%s/%s", c.endpoint, path), body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if authenticate {
		req.Header.Set("Authorization", fmt.Sprintf("Basic %s", c.basicAuth))
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var result map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		// only return error if status code isn't 2xx (some requests have no json response)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			return nil, err
		}
	}

	// check if this is an error response
	if result["errors"] != nil {
		return nil, errors.New(fmt.Sprintf("%+v", result["errors"]))
	}
	fmt.Printf("--- response: %+v\n", result)

	return result, nil
}

func (c *DataciteClient) GenerateDOI(prefix string) (string, error) {
	payload := fmt.Sprintf(`
	{
		"data": {
			"type": "dois",
			"attributes": {
				"prefix": "%s"
			}
		}
	}`, prefix)
	response, err := c.executeRequest("POST", "/dois", strings.NewReader(payload), true)
	if err != nil {
		return "", err
	}
	data, ok := response["data"].(map[string]interface{})
	if !ok {
		log.Println(response)
		return "", errors.New("failed parsing datacite REST API response")
	}
	attributes, ok := data["attributes"].(map[string]interface{})
	if !ok {
		log.Println(response)
		return "", errors.New("failed parsing datacite REST API response")
	}
	doi, ok := attributes["doi"].(string)
	if !ok {
		log.Println(response)
		return "", errors.New("failed parsing datacite REST API response")
	}

	return doi, nil
}

func (c *DataciteClient) UpdateDOI(doi string, url string, state PublicationState, resource schema.Resource) (map[string]interface{}, error) {
	xml, err := xml.Marshal(resource)
	if err != nil {
		return nil, err
	}
	encoded := base64.StdEncoding.EncodeToString(xml)
	fmt.Println(string(xml))
	payload := fmt.Sprintf(`
	{
		"data": {
			"id": "%s",
			"type": "dois",
			"attributes": {
				"event": "%s",
				"doi": "%s",
				"url": "%s",
				"xml": "%s"
			}
		}
	}`, doi, state, doi, url, encoded)

	response, err := c.executeRequest("PUT", fmt.Sprintf("/dois/%s", doi), strings.NewReader(payload), true)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *DataciteClient) DeleteDOI(doi string) error {
	_, err := c.executeRequest("DELETE", fmt.Sprintf("/dois/%s", doi), nil, true)
	return err
}

func NewClient(endpoint string, account string, password string) *DataciteClient {
	return &DataciteClient{
		endpoint:   endpoint,
		basicAuth:  base64.StdEncoding.EncodeToString([]byte(account + ":" + password)),
		httpClient: &http.Client{},
	}
}
